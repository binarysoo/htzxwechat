var testurl = 'http://localhost:8080/symptoms?illname=%E6%84%9F%E5%86%92';
var illname ={"erke":"儿科"
,"neike":"内科"
,"waike":"外科"
,"fuchanke":"妇产科"
,"wuguanke":"五官科"
,"changjianjibing":"常见疾病"};

var erke = [
  {
	sickname: "鼻塞",
	sdPerformance: ["有", "无"],
	sdId: [1, 4]
}, {
	sickname: "流涕",
	sdPerformance: ["流清涕", "流浊或黄涕"],
	sdId: [2, 3]
}, {
	sickname: "头痛",
	sdPerformance: ["有", "无"],
	sdId: [5, 6]
}, {
	sickname: "肢体酸痛",
	sdPerformance: ["有", "无"],
	sdId: [7, 8]
}, {
	sickname: "有痰",
	sdPerformance: ["吐稀薄白痰", "吐黄粘痰"],
	sdId: [9, 28]
}, {
	sickname: "咽痛",
 // items:[
 //   {sdId:10,sdname:"有"}
 // ]
	sdPerformance: ["有", "无"],
	sdId: [10, 11]
}, {
	sickname: "恶寒怕冷",
	sdPerformance: ["有", "无"],
	sdId: [130, 131]
}, {
	sickname: "咳嗽",
	sdPerformance: ["有", "无"],
	sdId: [132, 133]
}, {
	sickname: "发热",
	sdPerformance: ["有", "无"],
	sdId: [134, 135]
}, {
	sickname: "口干",
	sdPerformance: ["有", "无"],
	sdId: [136, 137]
}];

var wuguanke=[{
	"sickname": "多梦",
	"sdPerformance": ["有", "无"],
	"sdId": [96, 97]
}, {
	"sickname": "彻夜不眠",
	"sdPerformance": ["有", "无"],
	"sdId": [98, 99]
}, {
	"sickname": "易惊醒",
	"sdPerformance": ["有", "无"],
	"sdId": [100, 101]
}, {
	"sickname": "难以入眠但睡后到清晨",
	"sdPerformance": ["有", "无"],
	"sdId": [102, 103]
}, {
	"sickname": "睡后半夜即醒，难以再次入眠",
	"sdPerformance": ["有", "无"],
	"sdId": [104, 105]
}, {
	"sickname": "急躁易怒",
	"sdPerformance": ["有", "无"],
	"sdId": [106, 107]
}, {
	"sickname": "胸闷",
	"sdPerformance": ["有", "无"],
	"sdId": [108, 109]
}, {
	"sickname": "饮食",
	"sdPerformance": ["有", "无"],
	"sdId": [110, 111]
}, {
	"sickname": "口干口苦",
	"sdPerformance": ["有", "无"],
	"sdId": [112, 113]
}, {
	"sickname": "头晕目眩",
	"sdPerformance": ["有", "无"],
	"sdId": [114, 115]
}, {
	"sickname": "食欲",
	"sdPerformance": ["食少纳呆、腹胀", "无食欲"],
	"sdId": [116, 117]
}, {
	"sickname": "面色",
	"sdPerformance": ["苍白少华", "两颧潮红"],
	"sdId": [118, 119]
}, {
	"sickname": "精神状态",
	"sdPerformance": ["神疲乏力", "五心烦热", "触事易惊，终日惕惕"],
	"sdId": [120, 121, 122]
}, {
	"sickname": "四肢",
	"sdPerformance": ["四肢倦怠", "腰膝酸软", "倦怠乏力"],
	"sdId": [123, 124, 125]
}, {
	"sickname": "月经不调",
	"sdPerformance": ["有", "无"],
	"sdId": [126, 127]
}, {
	"sickname": "遗精",
	"sdPerformance": ["有", "无"],
	"sdId": [128, 129]
}];

var neike = [{
	"sickname": "疼痛类型",
	"sdPerformance": ["暴作", "胀满拒按", "胀痛连两胁", "痛势急迫，灼热", "痛如针刺，刀割，有定处", "隐隐灼痛", "隐隐不休"],
	"sdId": [12, 13, 14, 15, 16, 17, 18]
}, {
	"sickname": "影响疼痛因素",
	"sdPerformance": ["得温痛减，遇寒加重", "吐后痛减，矢气痛减", "遇烦加重，嗳气、矢气痛减", "按、食后加剧", "空腹、劳累、受凉加重，食后缓解"],
	"sdId": [19, 20, 21, 22, 23]
}, {
	"sickname": "饮食情况",
	"sdPerformance": ["口淡不渴、喜热饮", "不想吃饭", "口苦口干、口渴不想喝", "饥不欲食、口燥咽干、口渴思饮"],
	"sdId": [24, 25, 26, 27]
}, {
	"sickname": "呕吐情况",
	"sdPerformance": ["呕吐物为不消化食物", "纳呆恶心", "呕吐物为清水"],
	"sdId": [29, 30, 31]
}, {
	"sickname": "大便情况",
	"sdPerformance": ["不爽不畅", "或有黑便", "大便干结", "大便溏薄"],
	"sdId": [32, 33, 34, 35]
}, {
	"sickname": "喜叹气",
	"sdPerformance": ["有", "无"],
	"sdId": [36, 37]
}, {
	"sickname": "五心烦热，消瘦乏力",
	"sdPerformance": ["有", "无"],
	"sdId": [140, 141]
}, {
	"sickname": "神疲纳呆，四肢倦怠，手足不温",
	"sdPerformance": ["有", "无"],
	"sdId": [142, 143]
}];

var waike = [{
	"sickname": "疼痛部位",
	"sdPerformance": ["脐腹、少腹疼", "脐以上大腹疼痛", "脐以下小腹疼痛"],
	"sdId": [60, 61, 62]
}, {
	"sickname": "疼痛性质",
	"sdPerformance": ["腹痛拘急", "腹痛拒按", "腹痛胀满", "腹痛胀闷、痛无定处", "腹痛较剧、痛如针刺", "腹痛绵绵、时作时止"],
	"sdId": [63, 64, 65, 66, 67, 68]
}, {
	"sickname": "影响疼痛因素",
	"sdPerformance": ["得温痛减、遇寒加重", "遇热加重", "吐后痛减、矢气痛减", "遇烦加重、嗳气、矢气痛减", "食后加剧", "空腹、劳累加重，食后缓解"],
	"sdId": [69, 70, 71, 72, 73, 74]
}, {
	"sickname": "饮食、口渴情况",
	"sdPerformance": ["口淡不渴、喜热饮", "烦渴饮冷", "恶食", "口苦口干、口渴不想喝", "口渴思饮"],
	"sdId": [75, 76, 77, 78, 79]
}, {
	"sickname": "呕吐情况",
	"sdPerformance": ["有", "无"],
	"sdId": [80, 81]
}, {
	"sickname": "小便情况",
	"sdPerformance": ["小便清长", "小便短黄", "小便正常"],
	"sdId": [82, 83, 84]
}, {
	"sickname": "大便情况",
	"sdPerformance": ["大便清稀", "大便干燥", "大便秘结", "不欲排便", "排便无力", "痛而欲泻，泻后痛减"],
	"sdId": [85, 86, 87, 88, 89, 145]
}, {
	"sickname": "喜嗳气",
	"sdPerformance": ["有", "无"],
	"sdId": [90, 91]
}, {
	"sickname": "泻后痛减",
	"sdPerformance": ["有", "无"],
	"sdId": [92, 93]
}, {
	"sickname": "神疲乏力、少气懒言",
	"sdPerformance": ["有", "无"],
	"sdId": [94, 95]
}];

var fuchanke = [{
	"sickname": "大便色质",
	"sdPerformance": ["大便清稀甚至如水样", "完谷不化", "大便色黄褐而臭如败卵", "时溏时泻"],
	"sdId": [38, 39, 40, 41]
}, {
	"sickname": "病程",
	"sdPerformance": ["久泻、病程较长、泻下缓慢", "暴泻、病程短、泻下急迫"],
	"sdId": [42, 43]
}, {
	"sickname": "腹痛情况",
	"sdPerformance": ["泻下腹痛，痛势急迫拒按，泻后痛减", "腹痛不甚，喜温喜按"],
	"sdId": [44, 45]
}, {
	"sickname": "饮食情况",
	"sdPerformance": ["食少腹胀", "嗳腐吞酸、不思饮食", "烦热口渴", "口不渴，或渴不欲饮"],
	"sdId": [46, 47, 48, 49]
}, {
	"sickname": "恶寒发热、鼻塞头痛、肢体酸痛",
	"sdPerformance": ["有", "无"],
	"sdId": [50, 51]
}, {
	"sickname": "小便情况",
	"sdPerformance": ["小便清长", "小便短黄"],
	"sdId": [52, 53]
}, {
	"sickname": "神疲乏力，面色萎黄",
	"sdPerformance": ["有", "无"],
	"sdId": [54, 55]
}, {
	"sickname": "肛门灼热",
	"sdPerformance": ["无", "有"],
	"sdId": [57, 138]
}, {
	"sickname": "腰膝酸软",
	"sdPerformance": ["有", "无"],
	"sdId": [58, 59]
}, {
	"sickname": "形寒肢冷",
	"sdPerformance": ["有", "无"],
	"sdId": [56, 139]
}];


function change (){
  var res = Array();
  for(var sditem in qs){
    var meda ;
    meda.sickname = sditem.sickname;
    for(var i ;i<sditem.sdId.length;i++){
      var item = new Map();
      item.set(sditem.sdId[i],sditem.sdPerformance[i]);
      meda.items.add(item);  
    }
  }
}

var changjianjibing = [{
  "sickname": "常见疾病",
  "sdPerformance": ["感冒", "发烧", "腹痛", "头痛"],
  "sdId": [38, 39, 40, 41]
}, ];


function change() {
  var res = Array();
  for (var sditem in qs) {
    var meda;
    meda.sickname = sditem.sickname;
    for (var i; i < sditem.sdId.length; i++) {
      var item = new Map();
      item.set(sditem.sdId[i], sditem.sdPerformance[i]);
      meda.items.add(item);
    }
  }
}

function change (id){
	
  var res = Array();
  var qs;
  switch(id){
    case "erke":
    qs = erke;
    break;
    case "neike":
    qs = neike;
    break;
    case "waike":
    qs = waike;
    break;
    case "fuchanke":
    qs = fuchanke;
    break;
    case "wuguanke":
    qs = wuguanke;
    break;    
    case "changjianjibing":
    qs = changjianjibing;
    break;    
  }
  for(var k=0; k<qs.length;k++){
  	console.log(qs[k]);
    var meda = new Object() ;
    meda.sickname = qs[k].sickname;
    console.log("sname:"+meda.sickname);
    meda.items = new Array();
    for(var i=0 ;i<qs[k].sdId.length;i++){
      var item = new Object();
      item.sdid = qs[k].sdId[i];
      item.sdPerformance = qs[k].sdPerformance[i];
      meda.items.push(item); 
    }
    res.push(meda);
  }
  return res;
}

Page({

  /**
   * 页面的初始数据
   * mvn clean install
   */
  data:{
    datas: ''
  },
   
  
  makeRequest: function () {
    var self = this

    self.setData({
      loading: true
    })

    wx.request({
      url: testurl,
      data: {
        noncestr: Date.now()
      },
      success: function (result) {
        wx.showToast({
          title: '请求成功',
          icon: 'success',
          mask: true,
          duration: 100
        })
        self.setData({
          loading: false
        })
        console.log('request success', result)
      },

      fail: function ({ errMsg }) {
        console.log('request fail', errMsg)
        self.setData({
          loading: false
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(illname["futong"]);
    console.log(options.id);//获取url中的参数
    this.setData({
        datas: change(options.id)
    });

  }
})